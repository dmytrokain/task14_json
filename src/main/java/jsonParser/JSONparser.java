package jsonParser;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class JSONparser {
    private ObjectMapper objectMapper;
    private File json;

    public JSONparser() {
        this.objectMapper = new ObjectMapper();
    }

    public TreeSet<Flower> getFlowersList(String jsonPath) {
        json = new File(jsonPath);

        Flower[] flowers = new Flower[0];

        try {
            flowers = objectMapper.readValue(json, Flower[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new TreeSet<Flower>(Arrays.asList(flowers));
    }

    public static void main(String[] args) {
        JSONparser jsoNparser = new JSONparser();
        System.out.println(jsoNparser.getFlowersList("src/main/resources/flowerJSON.json"));
    }
}
